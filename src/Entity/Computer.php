<?php

namespace App\Entity;

use App\Repository\ComputerRepository;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Entity(repositoryClass=ComputerRepository::class)
 */
class Computer
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     * @Assert\NotBlank(message="Le modèle ne peut pas être null")
     */
    private $modele;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $marque;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $type;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $category;

    /**
     * @ORM\Column(type="integer")
     */
    private $prixAchat;

    /**
     * @ORM\Column(type="integer")
     */
    private $prixVente;

    /**
     * @ORM\Column(type="date")
     */
    private $dateEntreStock;

    /**
     * Computer constructor.
     * @param $modele
     * @param $marque
     * @param $type
     * @param $category
     * @param $prixAchat
     * @param $prixVente
     * @param $dateEntreStock
     */
    public function __construct($modele =  null, $marque = null, $type = null, $category = null, $prixAchat = null, $prixVente = null, $dateEntreStock = null)
    {
        $this->modele = $modele;
        $this->marque = $marque;
        $this->type = $type;
        $this->category = $category;
        $this->prixAchat = $prixAchat;
        $this->prixVente = $prixVente;
        $this->dateEntreStock = $dateEntreStock;
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getModele(): ?string
    {
        return $this->modele;
    }

    public function setModele(string $modele): self
    {
        $this->modele = $modele;

        return $this;
    }

    public function getMarque(): ?string
    {
        return $this->marque;
    }

    public function setMarque(string $marque): self
    {
        $this->marque = $marque;

        return $this;
    }

    public function getType(): ?string
    {
        return $this->type;
    }

    public function setType(string $type): self
    {
        $this->type = $type;

        return $this;
    }

    public function getCategory(): ?string
    {
        return $this->category;
    }

    public function setCategory(string $category): self
    {
        $this->category = $category;

        return $this;
    }

    public function getPrixAchat(): ?int
    {
        return $this->prixAchat;
    }

    public function setPrixAchat(int $prixAchat): self
    {
        $this->prixAchat = $prixAchat;

        return $this;
    }

    public function getPrixVente(): ?int
    {
        return $this->prixVente;
    }

    public function setPrixVente(int $prixVente): self
    {
        $this->prixVente = $prixVente;

        return $this;
    }

    public function getDateEntreStock()
    {
        return $this->dateEntreStock;
    }

    public function setDateEntreStock( $dateEntreStock): self
    {
        $this->dateEntreStock = $dateEntreStock;

        return $this;
    }

    public function toArray(){
        return [
            'id'=> $this->id,
            'category'=> $this->category,
            'modele'=> $this->modele,
            'marque'=> $this->marque,
            'type'=> $this->type,
            'prixAchat'=> $this->prixAchat,
            'prixVente'=> $this->prixVente,
            'dateEntreStock'=> $this->dateEntreStock
        ];
    }
}
